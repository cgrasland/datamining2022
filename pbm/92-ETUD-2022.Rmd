# Promotion 2021-2022 {#c04-API-Etudiants2022}

```{r}
## Global options
library(knitr)
library(dplyr)
library(ggplot2)
library(sf)



opts_chunk$set(echo=FALSE,
               cache=TRUE,
               prompt=FALSE,
               tidy=FALSE,
               comment=NA,
               message=FALSE,
               warning=FALSE)


```




