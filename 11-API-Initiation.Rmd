# Premiers pas   {#c11-API-def}

```{r}
library(knitr)
library(httr)
library(jsonlite)
library(insee)
library(dplyr)
library(lubridate)
```

## l'API de la NASA

A titre d'exemple, C. Pascual propose de travailler avec l'**API Open Notify**, qui donne accès à des données sur divers projets de la NASA. À l'aide de l'API Open Notify, nous pouvons notamment en savoir plus sur l'emplacement de la Station spatiale internationale et sur le nombre de personnes actuellement dans l'espace.


### Installer les packages jsonlite et httr

Pour travailler avec des API dans R, nous devons intégrer certaines bibliothèques (*library*). Ces bibliothèques prennent toutes les complexités d'une requête d'API et les enveloppent dans des fonctions que nous pouvons utiliser dans des lignes de code uniques. Les bibliothèques R que nous utiliserons sont `httr` et `jsonlite`. Elles remplissent des rôles différents dans notre introduction des API, mais les deux sont essentiels.Si vous ne disposez pas de ces bibliothèques dans votre console R ou RStudio, vous devez d'abord les télécharger.

```{r api1}
library(httr)
library(jsonlite)
```


### Formulation d'une requête GET()

Une requête adressé à une API va suivre le schéma suivant :

```{r api-req, fig.width=6}
knitr::include_graphics("img/API_GET.png",)
```


Il existe plusieurs types de requêtes que l'on peut adresser à un serveur API. Pour nos besoins, nous allons simplement demander des données, ce qui correspond à une demande **GET**. Les autres types de requêtes sont POST et PUT, mais nous n'avons pas à nous en préoccuper dans l'immédiat

Afin de créer une requête GET, nous devons utiliser la fonction *GET()* de la bibliothèque `httr`. La fonction GET() nécessite une URL, qui spécifie l'adresse du serveur auquel la demande doit être envoyée. 

Notre programme télécharge les données disponibles à l'adresse du serveur et les stocke dans un objet auquel on peut donner le nom que l'on souhaite, par exemple *ovni* dans la mesure où le résultat est de prime abord assez mystérieux...

```{r notif1}
ovni <- GET("http://api.open-notify.org/astros.json")
class(ovni)
```

On sait que la classe de l'objet est de type `response` ce qui ne nous avance pas beaucoup. 

Toutefois, si on demande à l'objet de s'afficher il nous apporte quatre renseignements utiles

```{r notif2}
ovni
```

- **Date** : le moment exact du téléchargement, très utile pour suivre les mises à jour
- **Status** : le code informatique de résultat de la requête. La valeur *200* indique un succès alors que les autres valeurs signaleront un problème.
- **Content-Type** : le type d'information recueillie. Ici, une application au format json
- **Size** : la taille du fichier résultant du transfert.

On poursuit notre enquête en tapant la commande *str()* qui permet d'avoir plus de détail sur le contenu de l'objet.

```{r notif3}
str(ovni)
```

Nous savons désormais que notre objet ovni est une **liste** comportant 10 branches, elles-mêmes divisées en sous branches qui peuvent être elles-même des listes... 

### Remarque sur les listes

Les listes sont des objets complexes mais fondamentaux pour la programmation en R. On peut accèder aux branches d'une liste soit en utilisant une série de `$` soit en se servant de doubles crochets `[[ ]]`. Par exemple, si on veut accèder à la date de la réponse on peut taper au choix :

```{r}
ovni$headers$date
ovni[["headers"]][["date"]]
```
On peut également afficher les noms des branches en partant de la racine puis en suivant l'arbre à l'aide de l'instruction `names()`

```{r}
names(ovni$headers)
```

```{r}
names(ovni$fields)
```
### Extraction des données

Les données contenues dans la réponse ont été stockées au format *JSON (JavaScript Object Notation)* qui est devenu un standard pour les échanges de données. Mais elles ont été ensuite comprimées en format binaire pour limiter la taille du fichier transféré. Il va donc falloir procéder en quatre étapes pour les extraire

#### étape 1 : récupérer les données au format binaire

On extrait le champ de données dans la liste. Le résultat est assez étrange : 

```{r}
don_bin<-ovni$content
don_bin
```

#### étape 2 : convertir les données binaires au format caractère

La conversion est effectuée à l'aide de la fonction `rawToChar()` qui fait partie de R-Base.

```{r }
# conversion du contenu de toto en mode character
don_car<-rawToChar(don_bin)
don_car
```
On commence à mieux voir le résultat mais ce n'est pas encore très lisible car il s'agit de données au format JSON

#### étape 3 : convertir les données JSON en objet R

On convertit les données de type JSON en données utilisables par R à l'aide de la fonction *fromJson()* du package `jsonlite()`

```{r notif4}
don_R <- fromJSON(don_car)
str(don_R)
```
On obtient finalement une liste de trois éléments dont le dernier est un *data.frame* décrivant les astronautes présents dans la station spatiale internationale au moment de l'execution du programme. 

#### étape 4 : Récupérer le tableau de résultats

```{r notif5}
tab<-don_R$people
kable(tab,caption = "Passagers de l'ISS en temps réel")
```

### Ecriture d'une fonction 

Une fois que l'on a bien compris la procédure d'extraction de cette API, on peut construire une fonction d'extraction pour simplifier la tâche et l'automatiser :

```{r}
## Fonction
extract_ISS <- function(){
  ovni <- GET("http://api.open-notify.org/astros.json") 
  don_bin<-ovni$content
  don_char<-rawToChar(don_bin)
  don_R<-fromJSON(don_char)
  tab<-don_R$people
  return(tab)
}

## Application
extract_ISS()
```

On peut améliorer la fonction en lui faisant ajouter un champ qui indique la date à laquelle a été effectué le relevé : 

```{r}
## Fonction
extract_ISS2 <- function(){
  ovni <- GET("http://api.open-notify.org/astros.json") 
  don_bin<-ovni$content
  don_char<-rawToChar(don_bin)
  don_R<-fromJSON(don_char)
  tab<-don_R$people
  tab$date<-ovni$headers$date
  return(tab)
}

## Application
extract_ISS2()
```
Et si on est à l'aise avec les listes, on peut aussi exporter les résultats sous la forme d'une liste plutôt que d'un tableau, ce qui évite de répéter plusieurs fois la date d'extraction des données


```{r}
## Fonction
extract_ISS3 <- function(){
  ovni <- GET("http://api.open-notify.org/astros.json") 
  don_bin<-ovni$content
  don_char<-rawToChar(don_bin)
  don_R<-fromJSON(don_char)
  tab<-don_R$people
  date<-ovni$headers$date
  result<-list("Update" = date,"Data" =tab)
  return(result)
}

## Application
x<-extract_ISS3()
kable(x$Data, caption=paste("Passagers de l'ISS :", x$Update))


```


### API et mise à jour en temps réel

Sur le site web du [billet proposé par C. Pascual en février 2020](https://www.dataquest.io/blog/r-api-tutorial/), on trouve une autre liste ne comportant que 6 passagers et avec des noms totalement différents :

```{r notif6,echo=FALSE}
craft<-rep("ISS",6)
name<-c("Christina Koch",   
 "Alexander Skvortsov",   
      "Luca Parmitano",   
       "Andrew Morgan",   
     "Oleg Skripochka",   
        "Jessica Meir") 
don<-data.frame(craft,name)
kable(don, caption = "Passagers de l'ISS en février 2020")
```

En effet, l'API renvoie les résultats au moment de l'execution de la fonction *GET()* ce qui correspond à février 2020 pour le billet de blog. Or, les astronautes sont remplacés au plus tous les six mois ce qui explique que tous les noms soient différents un an après. 

**NB : Cet exemple permet de mettre en évidence une fonction centrale des API qui est la mise à jour en temps réel des données !**


### API et requête paramétrique

L'exemple précédent consistait à télécharger la totalité d'un tableau et ne demandait donc pas de paramètres particuliers. Mais il peut aussi arriver (par exemple si une base de données est très volumineuse) que l'on précise à l'aide de paramètres ce que l'on veut précisément télécharger. 

A titre d'exemple, C. Pascual propose d'utiliser une autre API de la NASA intitulée [ISS Pass Time](http://open-notify.org/Open-Notify-API/ISS-Pass-Times/) qui permet de savoir à quel moment la station ISS passera au dessus d'un certain point du globe.

L'exemple choisi par C.Pascual est la recherche des trois prochaines dates de passage de l'ISS au dessus de New York dont les coordonnées de latitude et de longitude sont 40.7 et -74.0 :



```{r notif7}
titi <- GET("http://api.open-notify.org/iss-pass.json",
            query = list(lat = 40.7, lon = -74, n=3))
titi2 <- fromJSON(rawToChar(titi$content))
titi3 <- titi2$response
titi3
```

Le résultat paraît à première vue assez déconcertant. Mais la lecture de la documentation de l'API indique que les deux variables du tableau correspondent respectivement :

- *duration* : nombre de secondes pendant lesquelles la station sera à la verticale du point avec un angle de + ou - 10 degrés.
- *risetime* : moment de passage exprimé en [Unix Time](https://en.wikipedia.org/wiki/Unix_time) c'est-à-dire en nombre de secondes écoulées depuis le 1er Janvier 1970 UTC. 

Si l'on veut se ramener à une date précise, il faut donc convertir ce temps à l'aide d'une fonction R. Le plus simple est pour cela d'utiliser la fonction *as_datetime()* du package `lubridate`.

```{r notif8}
library(lubridate)
titi3$risetime<-as_datetime(titi3$risetime)
kable(titi3)
```


## Exercices

### Exercice 1 : utilisation de httr et jsonlite

Déterminer la durée et la date des 10 prochains dates de passage de l'ISS au dessus de Paris (Latitude = 48.86, Longitude = 2.35)

```{r notif9, echo=FALSE}

titi <- GET("http://api.open-notify.org/iss-pass.json",
            query = list(lat = 48.86, lon = 2.35, n=10))
titi2 <- fromJSON(rawToChar(titi$content))
titi3 <- titi2$response
titi3$risetime<-as_datetime(titi3$risetime)
kable(titi3, caption = "Prochains passages de l'ISS au dessus de Paris")
```

\iffalse
<div class="solution-exo">
```{r notif 10,  eval=FALSE}
titi <- GET("http://api.open-notify.org/iss-pass.json",
            query = list(lat = 48.86, lon = 2.35, n=10))
titi2 <- fromJSON(rawToChar(titi$content))
titi3 <- titi2$response
titi3$risetime<-as_datetime(titi3$risetime)
kable(titi3, caption = "Prochains passages de l'ISS au dessus de Paris")
```
</div>
\fi


